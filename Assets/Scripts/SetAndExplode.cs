using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class SetAndExplode : MonoBehaviour
{
    private ARRaycastManager raycastManager;
    private ARPlaneManager planeManager;
    private ARPointCloudManager pointCloudManager;

    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    [SerializeField] GameObject targetPrefab;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] ParticleSystem cubeExplosionPrefab;

    private bool targetPlaced;
    private bool bombPlaced;
    private bool bombActivated;
    private bool explosionHappened;

    private GameObject targetGameObject;
    private GameObject bombGameObject;

    Camera arCam;

    public float velocity;

    public UnityEngine.UI.Text Log;

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = GetComponent<ARRaycastManager>();
        planeManager = GetComponent<ARPlaneManager>();
        pointCloudManager = GetComponent<ARPointCloudManager>();

        targetPlaced = false;
        bombPlaced = false;
        bombActivated = false;
        explosionHappened = false;

        targetGameObject = null;
        bombGameObject = null;

        arCam = GameObject.Find("AR Camera").GetComponent<Camera>();

        Log.text = "initialized";
    }

    // Update is called once per frame
    void Update()
    {
        if (bombActivated && !bombGameObject.activeSelf && !targetGameObject.activeSelf && !explosionHappened)
        {
            Instantiate(cubeExplosionPrefab, targetGameObject.transform.position, Quaternion.identity);
            explosionHappened = true;
            return;
        }

        if (Input.touchCount == 0 || Input.GetTouch(0).phase != TouchPhase.Began)
        {
            return;
        }

        if (!targetPlaced)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.Planes))
            {
                targetGameObject = Instantiate(targetPrefab, hits[0].pose.position, Quaternion.identity);
                targetPlaced = true;

                planeManager.enabled = false;
                foreach (var Plane in planeManager.trackables)
                {
                    Plane.gameObject.SetActive(false);
                }
            }
            return;
        }

        if (!bombPlaced)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.All))
            {
                bombGameObject = Instantiate(bombPrefab, hits[0].pose.position, hits[0].pose.rotation);
                bombPlaced = true;

                pointCloudManager.enabled = false;
                foreach (var Point in pointCloudManager.trackables)
                {
                    Point.gameObject.SetActive(false);
                }
            }
            return;
        }

        if (targetPlaced && bombPlaced && !bombActivated)
        {
            if (raycastManager.Raycast(Input.GetTouch(0).position, hits))
            {
                RaycastHit hit;
                Ray ray = arCam.ScreenPointToRay(Input.GetTouch(0).position);

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.tag == "bomb")
                    {
                        bombGameObject.GetComponent<Light>().enabled = true;
                        bombActivated = true;
                    }
                }
            }
            return;
        }
        return;
    }

    private void FixedUpdate()
    {
        if (bombActivated)
        {
            Vector3 targetCoordinates = targetGameObject.transform.position;
            Vector3 bombCoordinates = bombGameObject.transform.position;
            Vector3 scaleVelocity = new Vector3();

            scaleVelocity.x = Math.Abs(targetCoordinates.x - bombCoordinates.x);
            scaleVelocity.y = Math.Abs(targetCoordinates.y - bombCoordinates.y);
            scaleVelocity.z = Math.Abs(targetCoordinates.z - bombCoordinates.z);

            float maxDistance = Mathf.Max(Mathf.Max(scaleVelocity.x, scaleVelocity.y), scaleVelocity.z);
            if (maxDistance > 0)
            {
                scaleVelocity.x = scaleVelocity.x / maxDistance;
                scaleVelocity.y = scaleVelocity.y / maxDistance;
                scaleVelocity.z = scaleVelocity.z / maxDistance;
            }

            if (Math.Abs(targetCoordinates.x - bombCoordinates.x) > 0.05 || Math.Abs(targetCoordinates.y - bombCoordinates.y) > 0.05 || Math.Abs(targetCoordinates.z - bombCoordinates.z) > 0.05)
            {
                Vector3 bombCoordinatesUpdate = new Vector3();

                if (bombCoordinates.x > targetCoordinates.x)
                {
                    bombCoordinatesUpdate.x = bombGameObject.transform.position.x - scaleVelocity.x * velocity * Time.deltaTime;
                }
                else
                {
                    bombCoordinatesUpdate.x = bombGameObject.transform.position.x + scaleVelocity.x * velocity * Time.deltaTime;
                }

                if (bombCoordinates.y > targetCoordinates.y)
                {
                    bombCoordinatesUpdate.y = bombGameObject.transform.position.y - scaleVelocity.y * velocity * Time.deltaTime;
                }
                else
                {
                    bombCoordinatesUpdate.y = bombGameObject.transform.position.y + scaleVelocity.y * velocity * Time.deltaTime;
                }

                if (bombCoordinates.z > targetCoordinates.z)
                {
                    bombCoordinatesUpdate.z = bombGameObject.transform.position.z - scaleVelocity.z * velocity * Time.deltaTime;
                }
                else
                {
                    bombCoordinatesUpdate.z = bombGameObject.transform.position.z + scaleVelocity.z * velocity * Time.deltaTime;
                }

                bombGameObject.transform.position = bombCoordinatesUpdate;
            }
        }
        return;
    }
}
